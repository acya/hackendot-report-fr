DOC = example
ASCIIDOC = $(DOC:=.asciidoc)
HTML = $(DOC:=.html)

all: $(HTML)

%.html: %.asciidoc docinfo-footer.html docinfo.html templates styles scripts
	asciidoctor -r asciidoctor-diagram -T ./templates/haml/html5/ -b html5 $<
